<?php

namespace Drupal\workflows_tutorial\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * Class ForceSettings.
 */
class ForceSettingsForm extends FormBase {
  // Force any settings needed for the tutorial.

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'workflows_force_settings';
  }

  /**
   * Gets the title dynamically.
   */
  public function getTitle() {
    $config = \Drupal::service('workflows_tutorial.status')->getConfig();
    return $config['title'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Get the forced values.
    $config = \Drupal::service('workflows_tutorial.status')->getConfig();

    $form['description'] = [
      '#type' => 'markup',
      '#markup' => '<p>' . $config['description'] . '</p><p>Please fill in the following values to get started.</p><hr>',
    ];

    foreach ($config['forced_settings'] as $key => $parts) {
      $form[$key] = [
        '#type' => 'textfield',
        '#title' => $parts['name'],
        '#description' => $parts['description'],
        '#required' => TRUE,
      ];
    }

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Let\'s get started!',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = \Drupal::service('workflows_tutorial.status')->getConfig();
    foreach ($config['forced_settings'] as $key => $parts) {
      $value = $form_state->getValue($key);
      \Drupal::configFactory()->getEditable($parts['config_space'])->set($parts['key'], $value)->save();
    }
    $form_state->setRedirect($config['first_route']);
  }
}
