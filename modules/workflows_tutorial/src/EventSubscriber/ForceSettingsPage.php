<?php

namespace Drupal\workflows_tutorial\EventSubscriber;

use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;

class ForceSettingsPage implements EventSubscriberInterface {

  public function checkForRedirection(RequestEvent $event) {
    // Check so logged in and not on setup page.
    if (\Drupal::currentUser()->isAuthenticated() && !\Drupal::service('workflows_tutorial.status')->isComplete()
      && !in_array($event->getRequest()->get('_route'), [
      'workflows_tutorial.forced_page',
      'user.logout',
      'user.login',
      'system.js_asset',
      'system.css_asset',
    ])) {
      \Drupal::messenger()->addMessage('You need to complete adding the keys before you can continue.');
      $event->setResponse(new RedirectResponse(Url::fromRoute('workflows_tutorial.forced_page')->toString(), 302));
    }
  }

 /**
  * {@inheritdoc}
  */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['checkForRedirection'];
    return $events;
  }

}
